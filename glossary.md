# Glossary

Merchant - The registered entity that is engaging with the customer, for bill payments
Transaction - Most of the times, it means 'Request'
Channel - Each merchant on the payer side can have multiple channels: physical stores, agents, apps, bank branches etc
Agent - Each unique entity in the channels mentioned above
