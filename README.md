# Federal Bank BBPS APIs
---

This is an attempt at understanding and documenting the [BBPS APIs](https://sandbox.federalbank.co.in/bbps) offered by Federal Bank, available "publicly" (yeah, you'll get that when you read more) at [Federal Bank developer portal](https://sandbox.federalbank.co.in/).

These APIs contain only the payment app / agent side of the BBPS, classified as a BBPOU and referred to as BBPS-COU.

Also contains a Postman collection, with requests and response samples from the sandbox saved as `examples` under each request.

## Code of Conduct

Please read our [Code Of Conduct](code_of_conduct.md).

## Licence

![CC BY 4.0 license button](https://licensebuttons.net/l/by/4.0/88x31.png "CC BY 4.0 license button")

This repository and all pieces of work under it are licensed under [Creative Commons 
Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/ "Creative Commons — Attribution-ShareAlike 4.0 International — CC BY 4.0") unless otherwise specified.

License snapshot is also available within the repo at [license](license).

---

##### Personal opinion
They should have named the sandbox as cement box, since all APIs return hardcoded responses which are sometimes not even the right data type.
