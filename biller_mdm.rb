# frozen_string_literal: true

require 'uri'
require 'openssl'
require 'net/http'
require 'json'

body_hash = {
  # identification of request type
  'RequestType' => 'GetMDMResponse',
  'MDMRequestDetails' => {
    'Commonfields' => {
      # identification of request type
      'RequestType' => 'GetMDMResponse',
      # random unique identifier on merchant end
      'MerchantRefNo' => 'rand_uniq_1',
      # merchant identification code provided by bank / BBPOU
      'MerchantCode' => 'bank_dep_1',
      # unique agent ID of the channel agent provided by bank / BBPOU
      'AgentId' => 'bank_dep_2',
      # user identification provided by bank / BBPOU
      'UserName' => 'bank_dep_3',
      # user password provided by bank / BBPOU
      'UserPass' => 'bank_dep_4',
      # store ID that is initiating this request - ATM/POS will provide Terminal ID, for other channels it will be same
      # as MerchantCode
      'StoreCode' => 'bank_dep_1',
      # set this to True to search for the provided BillerID, other cases undocumented
      'Searchmybiller' => 'True',
      # BillerID to search, no idea how to get the list
      'BillerId' => 'TSTV000000GUJ08',
      # undocumented, documented as array but sample value provided as string
      'EntityTypeId' => '5489401727549440',
      # channel details through which this txn happens
      'ChannelDetails' => [
        {
          # channel code value, effectively undocumented
          'ChannelCode' => 'INT',
          # reference to some internal documentation, each channel code has some specific properties that can go under
          # the param 'Name'. Max 10 'Name' & 'Value' pairs allowed.
          'ChannelParams' => [
            {
              'Name' => 'Requester IP',
              'Value' => '10.12.133.77'
            }
          ]
        }
      ]
    }
  }
}

url = URI('https://sandgateway.federalbank.co.in/bbps/biller_mdm')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_PEER

request = Net::HTTP::Post.new(url)
request['content-type'] = 'application/json'
request['accept'] = 'application/json'
request.body = body_hash.to_json

response = http.request(request)
resp = JSON.parse response.read_body

resp.each do |biller|
  puts biller['billerAliasName']
end
