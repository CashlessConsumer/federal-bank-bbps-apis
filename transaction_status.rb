# frozen_string_literal: true

require 'uri'
require 'openssl'
require 'net/http'
require 'json'

body_hash = {
  # identification of request type
  'RequestType' => 'some_identifier',
  'BBPSStatusRequestDetails' => {
    # random unique identifier on merchant end
    'MerchantRefNo' => 'rand_uniq_1',
    # merchant identification code provided by bank / BBPOU
    'MerchantCode' => 'bank_dep_1',
    # unique agent ID of the channel agent provided by bank / BBPOU
    'AgentId' => 'bank_dep_2',
    # user identification provided by bank / BBPOU
    'UserName' => 'bank_dep_3',
    # user password provided by bank / BBPOU
    'UserPass' => 'bank_dep_4',
    # random unique identifier on operator end, where operator is most likely the bank
    'PaymentRefNo' => 'mimbawdeav',
    # undocumented
    'ConsumerNo' => 'mopgomaj',
    # txn list - from date
    'FromDate' => '6/24/2113',
    # txn list - to date
    'ToDate' => '5/7/2098',
    # channel details through which this txn happens
    'ChannelDetails' => {
      # channel code value, effectively undocumented
      'ChannelCode' => 'INT',
      # reference to some internal documentation, each channel code has some specific properties that can go under
      # the param 'Name'. Max 10 'Name' & 'Value' pairs allowed.
      'ChannelParams' => [
        {
          'Name' => 'Requester IP',
          'Value' => '10.12.133.77'
        }
      ]
    },
    # merchant txn ref number, received during payment confirmation
    'ActualMerchantRefNo' => 'ohaibuwirunbacgu',
    # EuroNet ref number, received during payment confirmation
    'EuronetRefNo' => 'foinhawulmuw'
  }
}

url = URI('https://sandgateway.federalbank.co.in/bbps/transaction_status')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_PEER

request = Net::HTTP::Post.new(url)
request['content-type'] = 'application/json'
request['accept'] = 'application/json'
request.body = body_hash.to_json

response = http.request(request)
puts response.read_body
