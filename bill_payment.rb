# frozen_string_literal: true

require 'uri'
require 'openssl'
require 'net/http'
require 'json'

body_hash = {
  # identification of request type
  'RequestType' => 'some_identifier',
  'MerchantServiceRequestDetails' => {
    # identification of request type
    'RequestType' => 'some_identifier',
    # IP address of the request intiator
    'RequesterIP' => '131.51.76.41',
    # random unique identifier on merchant end
    'MerchantRefNo' => 'rand_uniq_1',
    # merchant identification code provided by bank / BBPOU
    'MerchantCode' => 'bank_dep_1',
    # unique agent ID of the channel agent provided by bank / BBPOU
    'AgentId' => 'bank_dep_2',
    # user identification provided by bank / BBPOU
    'UserName' => 'bank_dep_3',
    # user password provided by bank / BBPOU
    'UserPass' => 'bank_dep_4',
    # store ID that is initiating this request - ATM/POS will provide Terminal ID, for other channels it will be same
    # as MerchantCode
    'StoreCode' => 'bank_dep_1',
    # EuroNet is a payments and financial services tech provider
    # probably a reference number from a EuroNet system that Fed bank uses
    # merchant gets this ref from fetch bill response
    'EuronetRefNo' => 'from_prev_resp',
    # Txn amount in paisa
    'Amount' => '1560666234159104',
    # biller's ID to make payment against
    'BillerId' => '7534682782564352',
    # unique payment for bill token
    # merchant gets this ref from fetch bill response
    'BillPaymentToken' => 'from_prev_resp',
    # some kind of split payment config, undocumented
    'SplitPay' => 'kudesdajrujuvco',
    # some kind of split payment config, undocumented
    'SplitPayAmount' => '7609223577862144',
    # channel details through which this txn happens
    'ChannelDetails' => {
      # channel code value, effectively undocumented
      'ChannelCode' => 'INT',
      # reference to some internal documentation, each channel code has some specific properties that can go under
      # the param 'Name'. Max 10 'Name' & 'Value' pairs allowed.
      'ChannelParams' => [
        {
          'Name' => 'Requester IP',
          'Value' => '10.12.133.77'
        }
      ]
    },
    # customer details as name value pairs
    'CustomerProfile' => [
      {
        'name' => 'Name',
        'value' => 'John Doe'
      }
    ],
    # customer bill related references as name value pairs
    'SubscriptionDetails' => [
      {
        'name' => 'Period',
        'value' => 'Monthly'
      }
    ],
    # info about the payment
    'PaymentInformation' => {
      # payment mode / instrument used
      'PaymentMode' => 'fijet',
      # additional payment details
      'PaymentParams' => [
        {
          'name' => 'Network',
          'value' => 'VISA'
        }
      ]
    },
    # additional info tags defined by the biller as name value pairs
    'AdditionalInformation' => [
      {
        'name' => 'Lora Harrington',
        'value' => '28.83'
      }
    ],
    'BillDetail' => {
      # undocumented, most likely breakdown of bill items
      'AdditionalAmount' => [
        {
          'name' => 'Nancy Gordon',
          'value' => '86.35'
        }
      ],
      # customer name
      'CustomerName' => 'Tommy Myers',
      # bill amount in paisa
      'Amount' => '6156076898582528',
      # bill due date
      'DueDate' => '9/17/2112',
      # undocumented, most likely convenience fee charged to customer
      'CustConvFee' => 'senakeksom',
      # undocumented, most likely description of convenience fee charged to customer
      'CustConvDesc' => 'Nunhicle something soemthing something long text',
      # bill generation date
      'BillDate' => '9/7/2048',
      # bill number in the biller's system
      'BillNumber' => '480862321770496',
      # bill period of the fetched bill
      'BillPeriod' => 'varohg'
    },
    # customer's login name (?)
    'LoginName' => 'Dennis Morgan',
    # convenience fee charged by the COU
    'COUcustConvFee' => 30.53549581,
    # undocumented
    'OFFUSPay' => 'girade',
    # undocumented
    'Hash' => 'ziwmadevugouwwa'
  }
}

url = URI('https://sandgateway.federalbank.co.in/bbps/bill_payment')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_PEER

request = Net::HTTP::Post.new(url)
request['content-type'] = 'application/json'
request['accept'] = 'application/json'
request.body = body_hash.to_json

response = http.request(request)
puts response.read_body
